# Unity 中文语言包 zh-hans.po

## 介绍

本仓库提供了一个用于 Unity 引擎的中文语言包文件 `zh-hans.po`。该文件旨在帮助开发者将 Unity 编辑器界面和相关工具翻译为简体中文，从而提升中文用户的使用体验。

## 文件说明

- **文件名**: `zh-hans.po`
- **语言**: 简体中文 (zh-hans)
- **适用平台**: Unity 引擎

## 使用方法

1. **下载文件**: 点击仓库页面右上角的 "Code" 按钮，选择 "Download ZIP" 下载整个仓库，或者直接下载 `zh-hans.po` 文件。

2. **导入 Unity**: 将 `zh-hans.po` 文件放置在 Unity 项目的合适位置，通常是 `Assets/Localization` 目录下。

3. **配置 Unity**: 在 Unity 编辑器中，进入 `Edit > Preferences > Languages`，选择 `zh-hans` 作为默认语言。

4. **应用更改**: 重启 Unity 编辑器，界面将会显示为简体中文。

## 贡献

欢迎开发者对本语言包进行改进和完善。如果您发现任何翻译错误或遗漏，可以通过提交 Pull Request 或 Issue 来帮助我们改进。

## 许可证

本项目采用 [MIT 许可证](LICENSE)，您可以自由使用、修改和分发本项目中的文件。

## 联系我们

如果您有任何问题或建议，欢迎通过 Issue 或邮件联系我们。

---

感谢您使用 Unity 中文语言包！